package me.loganb1max.CustomEnchants.Objects;

import net.md_5.bungee.api.ChatColor;

public enum Rarity {
	Basic(ChatColor.AQUA + "Basic", ChatColor.AQUA.toString()),
	Rare(ChatColor.GREEN + "Rare", ChatColor.GREEN.toString()),
	Epic(ChatColor.GOLD + "Epic", ChatColor.GOLD.toString()),
	Legendary(ChatColor.RED + "Legendary", ChatColor.RED.toString());
	
	private String text;
	private String color;
	
	Rarity(String txt, String c) {
		this.text = txt;
		this.color = c;
	}
	
	@Override
	public String toString() {
		return this.text;
	}
	
	public String getColor() {
		return this.color;
	}
}
