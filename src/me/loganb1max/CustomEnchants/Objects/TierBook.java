package me.loganb1max.CustomEnchants.Objects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

//.setDisplayName(ChatColor.AQUA.toString() + "Basic Book" + ChatColor.GRAY + " (Right Click)")

public class TierBook {
	public static ItemStack getTier1Book() {
		ItemStack bBook = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta bBookMeta = bBook.getItemMeta();
		bBookMeta.setDisplayName(ChatColor.AQUA.toString() + "Basic Book" + ChatColor.GRAY + " (Right Click)");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Right click to examine");
		lore.add(ChatColor.GRAY.toString() + "a " + ChatColor.AQUA + "Basic " + ChatColor.GRAY + "book.");
		bBookMeta.setLore(lore);
		bBook.setItemMeta(bBookMeta);
		return bBook;
	}
	public static ItemStack getTier2Book() {
		ItemStack bBook = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta bBookMeta = bBook.getItemMeta();
		bBookMeta.setDisplayName(ChatColor.GREEN.toString() + "Rare Book" + ChatColor.GRAY + " (Right Click)");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Right click to examine");
		lore.add(ChatColor.GRAY.toString() + "a " + ChatColor.GREEN + "Rare " + ChatColor.GRAY + "book.");
		bBookMeta.setLore(lore);
		bBook.setItemMeta(bBookMeta);
		return bBook;
	}
	public static ItemStack getTier3Book() {
		ItemStack bBook = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta bBookMeta = bBook.getItemMeta();
		bBookMeta.setDisplayName(ChatColor.GOLD.toString() + "Epic Book" + ChatColor.GRAY + " (Right Click)");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Right click to examine");
		lore.add(ChatColor.GRAY.toString() + "a " + ChatColor.GOLD + "Epic " + ChatColor.GRAY + "book.");
		bBookMeta.setLore(lore);
		bBook.setItemMeta(bBookMeta);
		return bBook;
	}
	public static ItemStack getTier4Book() {
		ItemStack bBook = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta bBookMeta = bBook.getItemMeta();
		bBookMeta.setDisplayName(ChatColor.RED.toString() + "Legendary Book" + ChatColor.GRAY + " (Right Click)");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Right click to examine");
		lore.add(ChatColor.GRAY.toString() + "a " + ChatColor.RED + "Legendary " + ChatColor.GRAY + "book.");
		bBookMeta.setLore(lore);
		bBook.setItemMeta(bBookMeta);
		return bBook;
	}
}
