package me.loganb1max.CustomEnchants.Objects;

public interface CustomEnchantment {
	
	//String name;
	//int maxLevel;
	//Rarity rarity;
	//String description;
	//Gear gear;
	
	public String getName();
	public int getMaxLevel();
	public Rarity getRarity();
	public String getDescription();
	public Gear getGear();
}
