package me.loganb1max.CustomEnchants.Items;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Utils;
import me.loganb1max.CustomEnchants.Enchantment.EnchantmentManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;

public class ItemManager implements Listener{

	public static HashMap<CustomEnchantment, Integer> getEnchantments(ItemStack item) {
		HashMap<CustomEnchantment, Integer> enchants = new HashMap<CustomEnchantment, Integer>();
		EnchantmentManager em = Main.getInstance().getEnchantmentManager();
		if (item.hasItemMeta()) {
			if (item.getItemMeta().getLore() != null) {
				List<String> lore = item.getItemMeta().getLore();
				for (String s : lore) {
					String[] line = ChatColor.stripColor(s).split(" ");
					String l0 = line[0];
					String l1 = line[1];
					l1 = l1.replace("(", "");
					l1 = l1.replace(")", "");
					if (isEnchantment(l0)) {
						enchants.put(em.getEnchantment(l0), Utils.getIntFromNumeral(l1));
					}
				}
			}
		}
		return enchants;
	}

	public static boolean hasEnchantment(String name, ItemStack item) {
		boolean hasEnchant = false;
		if (item != null) {
			if (getEnchantments(item) != null) {
				for (CustomEnchantment ce : getEnchantments(item).keySet()) {
					if (ce.getName().equals(name)) {
						hasEnchant = true;
					}
				}
			}
		}
		return hasEnchant;
	}
	
	public static int getEnchantmentLevel(String name, ItemStack item) {
		int output = 0;
		if (hasEnchantment(name, item)) {
			for (CustomEnchantment ce : getEnchantments(item).keySet()) {
				if (ce.getName().contains(name)) {
					output = getEnchantments(item).get(ce);
				}
			}
		}
		return output;
	}

	public static void addEnchantment(String enchantName, int lvl, ItemStack item) {
		if (hasEnchantment(enchantName, item) == true) {
			removeEnchantment(enchantName, item);
			HashMap<CustomEnchantment, Integer> enchants = getEnchantments(item);
			EnchantmentManager em = Main.getInstance().getEnchantmentManager();
			enchants.put(em.getEnchantment(enchantName), lvl);
			writeEnchantments(enchants, item);
		} else if (hasEnchantment(enchantName, item) == false) {
			HashMap<CustomEnchantment, Integer> enchants = getEnchantments(item);
			EnchantmentManager em = Main.getInstance().getEnchantmentManager();
			enchants.put(em.getEnchantment(enchantName), lvl);
			writeEnchantments(enchants, item);
		}
	}

	public static void removeEnchantment(String enchantName, ItemStack item) {
		if (hasEnchantment(enchantName, item) == true) {
			HashMap<CustomEnchantment, Integer> enchants = getEnchantments(item);
			EnchantmentManager em = Main.getInstance().getEnchantmentManager();
			enchants.remove(em.getEnchantment(enchantName));
		}
	}

	private static void writeEnchantments(HashMap<CustomEnchantment, Integer> enchants, ItemStack item) {
		ItemMeta meta = item.getItemMeta();
		List<String> lore = new ArrayList<String>();
		for (Entry<CustomEnchantment, Integer> set : enchants.entrySet()) {
			CustomEnchantment e = (CustomEnchantment) set.getKey();
			int lvl = (int) set.getValue();
			lore.add(e.getRarity().getColor() + e.getName() + " " + ChatColor.DARK_GRAY.toString() + "(" + ChatColor.GOLD + Utils.getNumeral(lvl) + ChatColor.DARK_GRAY + ")");
		}
		meta.setLore(lore);
		item.setItemMeta(meta);
	}

	private static boolean isEnchantment(String name) {
		boolean isEnchantment = false;
		EnchantmentManager em = Main.getInstance().getEnchantmentManager();
		for (CustomEnchantment ce : em.getEnchantmentsList()) {
			if (ce.getName().equals(ChatColor.stripColor(name))) {
				isEnchantment = true;
			}
		}
		return isEnchantment;
	}
	
	private boolean canEnchant(String name, ItemStack item) {
		boolean canEnchant = false;
		EnchantmentManager em = Main.getInstance().getEnchantmentManager();
		CustomEnchantment ce = em.getEnchantment(name);
		if (ce.getGear().containsItem(item)) {
			canEnchant = true;
		}
		return canEnchant;
	}
	
	public static boolean isEnchant(ItemStack item) {
		boolean isEnchant = false;
		if (item.getType().equals(Material.ENCHANTED_BOOK)) {
			ItemMeta meta = item.getItemMeta();
			String name = meta.getDisplayName().split(" ")[0];
			if (isEnchantment(name) == true) {
				isEnchant = true;
			}
		}
		return isEnchant;
	}
	
	public static List<Object> getEnchantInfo(ItemStack item) {
		List<Object> enchantInfo = new ArrayList<Object>();
		if (isEnchant(item) == true) {
			ItemMeta meta = item.getItemMeta();
			String[] nameLine = meta.getDisplayName().split(" ");
			String name = nameLine[0];
			int lvl = Utils.getIntFromNumeral(ChatColor.stripColor(nameLine[1]).replace("(",  "").replace(")", ""));
			List<String> lore = meta.getLore();
			String successRateString = ChatColor.stripColor(lore.get(3));
			successRateString = successRateString.replace("- Success Rate: ", "");
			successRateString = successRateString.replace("%", "");
			int successRate = Integer.parseInt(successRateString);
			enchantInfo.add(name);
			enchantInfo.add(lvl);
			enchantInfo.add(successRate);
		}
		return enchantInfo;
	}
	
	private static boolean isWhiteScroll(ItemStack item) {
		boolean isWhite = false;
		if (item.getType().equals(Material.PAPER)) {
			if (item.hasItemMeta()) {
				ItemMeta meta = item.getItemMeta();
				if (meta.getDisplayName().equals(ChatColor.WHITE + "White Scroll")) {
					isWhite = true;
				}
			}
		}
		return isWhite;
	}
	
	private static boolean isBlackScroll(ItemStack item) {
		boolean isBlack = false;
		if (item.getType().equals(Material.PAPER)) {
			if (item.hasItemMeta()) {
				ItemMeta meta = item.getItemMeta();
				if (meta.getDisplayName().equals(ChatColor.WHITE + "Black Scroll")) {
					isBlack = true;
				}
			}
		}
		return isBlack;
	}
	
	private static ItemStack blackScrollItem(ItemStack item, ItemStack scroll) {
		ItemStack book;
		Random rand = new Random();
		HashMap<CustomEnchantment, Integer> enchants = getEnchantments(item);
		CustomEnchantment ce = (CustomEnchantment) enchants.keySet().toArray()[rand.nextInt(enchants.size())];
		int lvl = enchants.get(ce);
		enchants.remove(ce);
		writeEnchantments(enchants, item);
		EnchantmentManager em = Main.getInstance().getEnchantmentManager();
		ItemMeta scrollMeta = scroll.getItemMeta();
		List<String> lore = scrollMeta.getLore();
		int percentage = Integer.parseInt(lore.get(2).replace(ChatColor.GRAY + "it into a " + ChatColor.WHITE.toString(), "").replace("%" + ChatColor.GRAY + " success book.", ""));
		book = em.generateEnchantment(ce.getName(), lvl, ce.getRarity(), ce.getDescription(), ce.getGear(), percentage);
		return book;
	}
	
	private static boolean isProtected(ItemStack item) {
		boolean isProtected = false;
		if (item.hasItemMeta() == true) {
			ItemMeta meta = item.getItemMeta();
			if (meta.getDisplayName() != null) {
				if (meta.getDisplayName().contains(ChatColor.GREEN + "PROTECTED")) {
					isProtected = true;
				}
			}
		}
		return isProtected;
	}
	
	private static void protectItem(ItemStack item) {
		if (item.hasItemMeta()) {
			ItemMeta meta = item.getItemMeta();
			if (meta.getDisplayName() != null) {
				meta.setDisplayName(ChatColor.GREEN + "PROTECTED " + meta.getDisplayName());
				item.setItemMeta(meta);
			} else {
				String name = Utils.fixName(item.getType().toString());
				meta.setDisplayName(ChatColor.GREEN + "PROTECTED " + ChatColor.RESET +  name);
				item.setItemMeta(meta);
			}
		}
	}
	
	private static void unProtectItem(ItemStack item) {
		if (item.hasItemMeta()) {
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(meta.getDisplayName().replace(ChatColor.GREEN + "PROTECTED ", ""));
			item.setItemMeta(meta);
		}
	}
	
	public static ItemStack generateWhiteScroll() {
		ItemStack scroll = new ItemStack(Material.PAPER);
		ItemMeta meta = scroll.getItemMeta();
		meta.setDisplayName(ChatColor.WHITE + "White Scroll");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Prevents an item from being destroyed");
		lore.add(ChatColor.GRAY + "due to a failed enchantment book.");
		lore.add(ChatColor.WHITE + "Place scroll on item to apply.");
		meta.setLore(lore);
		scroll.setItemMeta(meta);
		return scroll;
	}
	
	public static ItemStack generateBlackScroll(int chance) {
		ItemStack scroll = new ItemStack(Material.PAPER);
		ItemMeta meta = scroll.getItemMeta();
		meta.setDisplayName(ChatColor.WHITE + "Black Scroll");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "Removes a random enchantment");
		lore.add(ChatColor.GRAY + "from an item and converts");
		lore.add(ChatColor.GRAY + "it into a " + ChatColor.WHITE.toString() + chance + "%" + ChatColor.GRAY + " success book.");
		lore.add(ChatColor.WHITE + "Place scroll on item to extract.");
		meta.setLore(lore);
		scroll.setItemMeta(meta);
		return scroll;
	}
	
	public static ItemStack generateRandomBlackScroll() {
		Random rand = new Random();
		return generateBlackScroll(rand.nextInt(99)+1);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEnchant(InventoryClickEvent e) {
		if (e.getSlotType().equals(SlotType.OUTSIDE) || e.getSlotType().equals(SlotType.ARMOR)) {
			return;
		}
		if (e.getCursor() == null) {
			return;
		}
		if (e.getCurrentItem() == null) {
			return;
		}
		Player p = (Player)e.getWhoClicked();
		if (isEnchant(e.getCursor()) == true) {
			List<Object> eInfo = getEnchantInfo(e.getCursor());
			String name = ChatColor.stripColor((String) eInfo.get(0));
			int lvl = (int) eInfo.get(1);
			int success = (int) eInfo.get(2);
			boolean whiteScroll = isProtected(e.getCurrentItem());
			Random rand = new Random();
			if (canEnchant(name, e.getCurrentItem()) == true) {
				int outcome = rand.nextInt(100);
				if (whiteScroll == true) {
					if (outcome <= success) {
						addEnchantment(name, lvl, e.getCurrentItem());
						p.playSound(p.getLocation(), Sound.SUCCESSFUL_HIT, 1, 1);
						unProtectItem(e.getCurrentItem());
						e.setCursor(null);
						e.setCancelled(true);
					} else if (outcome > success) {
						p.playSound(p.getLocation(), Sound.FIZZ, 1, 1);
						unProtectItem(e.getCurrentItem());
						e.setCursor(null);
						e.setCancelled(true);
					}
				} else if (whiteScroll == false) {
					if (outcome <= success) {
						addEnchantment(name, lvl, e.getCurrentItem());
						p.playSound(p.getLocation(), Sound.SUCCESSFUL_HIT, 1, 1);
						e.setCursor(null);
						e.setCancelled(true);
					} else if (outcome > success) {
						p.playSound(p.getLocation(), Sound.FIZZ, 1, 1);
						e.setCursor(null);
						e.setCurrentItem(null);
						e.setCancelled(true);
					}
				}
			}
		}
		if (isWhiteScroll(e.getCursor()) == true) {
			if (Gear.Armour.containsItem(e.getCurrentItem()) || Gear.Bow.containsItem(e.getCurrentItem()) || Gear.SwordAndAxe.containsItem(e.getCurrentItem()) || Gear.Pickaxe.containsItem(e.getCurrentItem())) {
				if (isProtected(e.getCurrentItem()) == false) {
					protectItem(e.getCurrentItem());
					e.setCursor(null);
					e.setCancelled(true);
				}
			}
		}
		if (isBlackScroll(e.getCursor()) == true) {
			if (Gear.Armour.containsItem(e.getCurrentItem()) || Gear.Bow.containsItem(e.getCurrentItem()) || Gear.SwordAndAxe.containsItem(e.getCurrentItem()) || Gear.Pickaxe.containsItem(e.getCurrentItem())) {
				if (getEnchantments(e.getCurrentItem()).size() > 0) {
					p.sendMessage(ChatColor.GREEN.toString() + ChatColor.BOLD + "(!) " + ChatColor.GREEN + "You have blackscrolled your item!");
					ItemStack book = blackScrollItem(e.getCurrentItem(), e.getCursor());
					p.getInventory().addItem(book);
					e.setCursor(null);
					e.setCancelled(true);
				}
			}
		}
	}
}
