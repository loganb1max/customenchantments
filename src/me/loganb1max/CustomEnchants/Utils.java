package me.loganb1max.CustomEnchants;

public class Utils {

	public static String getNumeral(int i) {
		switch(i) {
		case 1:
			return "I";
		case 2:
			return "II";
		case 3:
			return "III";
		case 4:
			return "IV";
		case 5:
			return "V";
		case 6:
			return "VI";
		case 7:
			return "VII";
		case 8:
			return "VIII";
		case 9:
			return "IX";
		case 10:
			return "X";
		}
		return null;
	}
	
	public static int getIntFromNumeral(String numeral) {
		switch(numeral) {
		case "I":
			return 1;
		case "II":
			return 2;
		case "III":
			return 3;
		case "IV":
			return 4;
		case "V":
			return 5;
		case "VI":
			return 6;
		case "VII":
			return 7;
		case "VIII":
			return 8;
		case "IX":
			return 9;
		case "X":
			return 10;
		}
		return 0;
	}
	
	public static String fixName(String input) {
		String fixed = "";
		input = input.toLowerCase();
		String[] words = input.split("_");
		for (String word : words) {
			word = word.replace(String.valueOf(word.charAt(0)), String.valueOf(word.charAt(0)).toUpperCase());
			fixed += word + " ";
		}
		fixed.trim();
		
		return fixed;
	}
	
}
