package me.loganb1max.CustomEnchants;

import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import me.loganb1max.CustomEnchants.Enchantment.EnchantmentManager;
import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin{

	private static Main instance;
	private Initializer start;
	private EnchantmentManager eManager;
	private Economy econ = null;
	
	public void onEnable() {
		instance = this;
		start = new Initializer(instance);
		start.registerListeners();
		start.registerCommands();
		eManager = new EnchantmentManager();
		start.registerEnchantments();
		setupEconomy();
	}
	
	public EnchantmentManager getEnchantmentManager() {
		return this.eManager;
	}
	
	public static Main getInstance() {
		return instance;
	}
	
	private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
	
	public Economy getEcon() {
		return this.econ;
	}
	
	
}
