package me.loganb1max.CustomEnchants.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import me.loganb1max.CustomEnchants.GUI.EnchantmentsGUI;

public class CustomEnchantsCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player p =  (Player) sender;
			EnchantmentsGUI.openEnchantmentsGui(p);
			return true;
		}
		return false;
	}

}
