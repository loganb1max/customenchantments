package me.loganb1max.CustomEnchants.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.GUI.EnchanterGUI;
import me.loganb1max.CustomEnchants.Items.ItemManager;

public class CustomEnchantCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player p =  (Player) sender;
			if (args.length < 1) {
				EnchanterGUI.showEnchantGui(p);
				return true;
			}
			if (args.length == 3) {
				if (args[0].toLowerCase().equals("give")) {
					if (Bukkit.getPlayer(args[1]) != null) {
						Player t = Bukkit.getPlayer(args[1]);
						if (args[2].toLowerCase().equals("whitescroll")) {
							if (p.hasPermission("customenchantments.admin")) {
								t.getInventory().addItem(ItemManager.generateWhiteScroll());
								return true;
							}
						} else if (args[2].toLowerCase().equals("blackscroll")) {
							if (p.hasPermission("customenchantments.admin")) {
								t.getInventory().addItem(ItemManager.generateRandomBlackScroll());
								return true;
							}
						} else if (args[2].toLowerCase().equals("basic")) {
							if (p.hasPermission("customenchantments.admin")) {
								t.getInventory().addItem(Main.getInstance().getEnchantmentManager().getRandomEnchantment(1));
								return true;
							}
						} else if (args[2].toLowerCase().equals("rare")) {
							if (p.hasPermission("customenchantments.admin")) {
								t.getInventory().addItem(Main.getInstance().getEnchantmentManager().getRandomEnchantment(2));
								return true;
							}
						} else if (args[2].toLowerCase().equals("epic")) {
							if (p.hasPermission("customenchantments.admin")) {
								t.getInventory().addItem(Main.getInstance().getEnchantmentManager().getRandomEnchantment(3));
								return true;
							}
						} else if (args[2].toLowerCase().equals("legendary")) {
							if (p.hasPermission("customenchantments.admin")) {
								t.getInventory().addItem(Main.getInstance().getEnchantmentManager().getRandomEnchantment(4));
								return true;
							}
						} else {
							if (p.hasPermission("customenchantments.admin")) {
								p.sendMessage(ChatColor.RED + "invalid syntax! /" + label + " or /" + label +" give <Player> <whitescroll, blackscroll, enchantrarity>");
								return true;
							} else {
								p.sendMessage(ChatColor.RED + "invalid syntax! /" + label);
								return true;
							}
						}
					}
				} else {
					if (p.hasPermission("customenchantments.admin")) {
						p.sendMessage(ChatColor.RED + "invalid syntax! /" + label + " or /" + label +" give <Player> <whitescroll, blackscroll, enchantrarity>");
					} else {
						p.sendMessage(ChatColor.RED + "invalid syntax! /" + label);
					}
				}
			}
			if (args.length > 3 || args.length == 1 || args.length == 2) {
				if (p.hasPermission("customenchantments.admin")) {
					p.sendMessage(ChatColor.RED + "invalid syntax! /" + label + " or /" + label +" give <Player> <whitescroll, blackscroll, enchantrarity>");
				} else {
					p.sendMessage(ChatColor.RED + "invalid syntax! /" + label);
				}
			}
		}
		return false;
	}

}
