package me.loganb1max.CustomEnchants.Enchantment;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Utils;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class EnchantmentManager implements Listener{

	private ArrayList<CustomEnchantment> enchantments;
	private ArrayList<CustomEnchantment> basicEnchantments;
	private ArrayList<CustomEnchantment> rareEnchantments;
	private ArrayList<CustomEnchantment> epicEnchantments;
	private ArrayList<CustomEnchantment> legendaryEnchantments;
	
	public EnchantmentManager() {
		enchantments = new ArrayList<CustomEnchantment>();
		basicEnchantments = new ArrayList<CustomEnchantment>();
		rareEnchantments = new ArrayList<CustomEnchantment>();
		epicEnchantments = new ArrayList<CustomEnchantment>();
		legendaryEnchantments = new ArrayList<CustomEnchantment>();
	}
	
	public ArrayList<CustomEnchantment> getEnchantmentsList() {
		return enchantments;
	}
	
	public CustomEnchantment getEnchantment(String name) {
		CustomEnchantment e = null;
		for (CustomEnchantment ce : enchantments) {
			if (ce.getName().equals(name)) {
				e = ce;
			}
		}
		return e;
	}
	
	public ItemStack generateEnchantment(String name, int l, Rarity r, String desc, Gear g, int success) {
		ItemStack book = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta bookMeta = book.getItemMeta();
		bookMeta.setDisplayName(r.getColor() + name + ChatColor.DARK_GRAY.toString() + " (" + ChatColor.GOLD + Utils.getNumeral(l) + ChatColor.DARK_GRAY +")");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY.toString() + "- " + ChatColor.GOLD + "Rarity: " + r.toString());
		lore.add(ChatColor.GRAY.toString() + "- " + ChatColor.GOLD + "Description: " + ChatColor.GRAY + desc);
		lore.add(ChatColor.GRAY.toString() + "- " + ChatColor.GOLD + "Gear: " + ChatColor.GRAY + g.toString());
		lore.add(ChatColor.GRAY.toString() + "- " + ChatColor.GOLD + "Success Rate: " + ChatColor.GREEN + success + "%");
		lore.add(ChatColor.GRAY.toString() + "- " + ChatColor.GOLD + "Destroy Rate: " + ChatColor.RED + (100-success) + "%");
		bookMeta.setLore(lore);
		bookMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		book.setItemMeta(bookMeta);
		return book;
	}
	
	public ItemStack generateEnchantmentInfoCard(String name, int maxLvl, Rarity r, String desc, Gear g) {
		ItemStack book = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta bookMeta = book.getItemMeta();
		bookMeta.setDisplayName(r.getColor() + name + ChatColor.DARK_GRAY.toString() + " (" + ChatColor.GOLD  + Utils.getNumeral(1) + ChatColor.GRAY + " - " + ChatColor.GOLD + Utils.getNumeral(maxLvl) + ChatColor.DARK_GRAY + ")");
		ArrayList<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY.toString() + "- " + ChatColor.GOLD + "Rarity: " + r.toString());
		lore.add(ChatColor.GRAY.toString() + "- " + ChatColor.GOLD + "Description: " + ChatColor.GRAY + desc);
		lore.add(ChatColor.GRAY.toString() + "- " + ChatColor.GOLD + "Gear: " + ChatColor.GRAY + g.toString());
		bookMeta.setLore(lore);
		book.setItemMeta(bookMeta);
		return book;
	}
	
	public ItemStack getRandomEnchantment(int tier) {
		ItemStack book = null;
		CustomEnchantment ce;
		Random rand = new Random();
		switch(tier){
			case 1:
				ce = basicEnchantments.get(rand.nextInt(basicEnchantments.size()));
				book = generateEnchantment(ce.getName(), rand.nextInt(ce.getMaxLevel())+1, ce.getRarity(), ce.getDescription(), ce.getGear(), rand.nextInt(100)+1);
				break;
			case 2:
				ce = rareEnchantments.get(rand.nextInt(rareEnchantments.size()));
				book = generateEnchantment(ce.getName(), rand.nextInt(ce.getMaxLevel())+1, ce.getRarity(), ce.getDescription(), ce.getGear(), rand.nextInt(100)+1);
				break;
			case 3:
				ce = epicEnchantments.get(rand.nextInt(epicEnchantments.size()));
				book = generateEnchantment(ce.getName(), rand.nextInt(ce.getMaxLevel())+1, ce.getRarity(), ce.getDescription(), ce.getGear(), rand.nextInt(100)+1);
				break;
			case 4:
				ce = legendaryEnchantments.get(rand.nextInt(legendaryEnchantments.size()));
				book = generateEnchantment(ce.getName(), rand.nextInt(ce.getMaxLevel())+1, ce.getRarity(), ce.getDescription(), ce.getGear(), rand.nextInt(100)+1);
				break;
		}
		return book;
	}
	
	public void registerEnchantment(CustomEnchantment ce) {
		if (ce != null) {
			enchantments.add(ce);
			if (ce.getRarity().equals(Rarity.Basic)) {
				basicEnchantments.add(ce);
			} else if (ce.getRarity().equals(Rarity.Rare)) {
				rareEnchantments.add(ce);
			} else if (ce.getRarity().equals(Rarity.Epic)) {
				epicEnchantments.add(ce);
			} else if (ce.getRarity().equals(Rarity.Legendary)) {
				legendaryEnchantments.add(ce);
			}
		}
	}
	
	
	@EventHandler
	public void onBookInspect(PlayerInteractEvent e) {
		if (e.getItem() != null) {
			if (e.getItem().getType().equals(Material.ENCHANTED_BOOK)) {
				if (e.getItem().getItemMeta().getDisplayName().contains("Basic Book")) {
					e.getPlayer().setItemInHand(null);
					e.getPlayer().getInventory().addItem(Main.getInstance().getEnchantmentManager().getRandomEnchantment(1));
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.FIREWORK_LARGE_BLAST, 1, 1);
					e.getPlayer().sendMessage(ChatColor.GRAY + "You've found a enchantment!");
					e.setCancelled(true);
				} else if (e.getItem().getItemMeta().getDisplayName().contains("Rare Book")) {
					e.getPlayer().setItemInHand(null);
					e.getPlayer().getInventory().addItem(Main.getInstance().getEnchantmentManager().getRandomEnchantment(2));
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.FIREWORK_LARGE_BLAST, 1, 1);
					e.getPlayer().sendMessage(ChatColor.GRAY + "You've found a enchantment!");
					e.setCancelled(true);
				} else if (e.getItem().getItemMeta().getDisplayName().contains("Epic Book")) {
					e.getPlayer().setItemInHand(null);
					e.getPlayer().getInventory().addItem(Main.getInstance().getEnchantmentManager().getRandomEnchantment(3));
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.FIREWORK_LARGE_BLAST, 1, 1);
					e.getPlayer().sendMessage(ChatColor.GRAY + "You've found a enchantment!");
					e.setCancelled(true);
				} else if (e.getItem().getItemMeta().getDisplayName().contains("Legendary Book")) {
					e.getPlayer().setItemInHand(null);
					e.getPlayer().getInventory().addItem(Main.getInstance().getEnchantmentManager().getRandomEnchantment(4));
					e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.FIREWORK_LARGE_BLAST, 1, 1);
					e.getPlayer().sendMessage(ChatColor.GRAY + "You've found a enchantment!");
					e.setCancelled(true);
				}
			}
		}
	}
	
}
