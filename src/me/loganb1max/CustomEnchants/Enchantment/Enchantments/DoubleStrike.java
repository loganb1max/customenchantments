package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class DoubleStrike implements CustomEnchantment, Listener{

	private String name = "DoubleStrike";
	private String description = "Chance to apply extra damage.";
	private Rarity rarity = Rarity.Rare;
	private Gear gear = Gear.Sword;
	private int maxLvl = 4;
	
	public DoubleStrike() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player) {
			Player p = (Player) e.getDamager();
			if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
				int lvl = ItemManager.getEnchantmentLevel(getName(), p.getItemInHand());
				Random rand = new Random();
				switch (lvl) {
				case 1:
					if (rand.nextInt(99)+1 < 10) {
						e.setDamage(e.getDamage() * 1.25);
					}
				case 2:
					if (rand.nextInt(99)+1 < 20) {
						e.setDamage(e.getDamage() * 1.5);
					}
				case 3:
					if (rand.nextInt(99)+1 < 30) {
						e.setDamage(e.getDamage() * 1.75);
					}
				case 4:
					if (rand.nextInt(99)+1 < 40) {
						e.setDamage(e.getDamage() * 2.0);
					}
				}
			}
		}
	}
}
