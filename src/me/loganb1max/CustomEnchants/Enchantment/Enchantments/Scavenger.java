package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Scavenger implements CustomEnchantment, Listener{

	private String name = "Scavenger";
	private String description = "Chance to increase luck of drops.";
	private Rarity rarity = Rarity.Basic;
	private Gear gear = Gear.Pickaxe;
	private int maxLvl = 3;
	
	public Scavenger() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if (ItemManager.hasEnchantment(getName(), p.getItemInHand()) && !ItemManager.hasEnchantment("AutoSmelt", p.getItemInHand())) {
			if (e.getBlock().getType().equals(Material.DIAMOND_ORE) || e.getBlock().getType().equals(Material.EMERALD_ORE) || e.getBlock().getType().equals(Material.COAL_ORE) || e.getBlock().getType().equals(Material.GLOWING_REDSTONE_ORE) || e.getBlock().getType().equals(Material.GOLD_ORE) || e.getBlock().getType().equals(Material.IRON_ORE) || e.getBlock().getType().equals(Material.LAPIS_ORE) || e.getBlock().getType().equals(Material.QUARTZ_ORE) || e.getBlock().getType().equals(Material.REDSTONE_ORE) || e.getBlock().getType().equals(Material.GLOWSTONE)) {
				int lvl = ItemManager.getEnchantmentLevel(getName(), e.getPlayer().getItemInHand());
				Random rand = new Random();
				switch(lvl) {
				case 1:
					if (rand.nextInt(99)+1 < 10) {
						for (ItemStack i : e.getBlock().getDrops()) {
							e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), i);
						}
					}
				case 2:
					if (rand.nextInt(99)+1 < 20) {
						for (ItemStack i : e.getBlock().getDrops()) {
							e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), i);
							e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), i);
						}
					}
				case 3:
					if (rand.nextInt(99)+1 < 30) {
						for (ItemStack i : e.getBlock().getDrops()) {
							e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), i);
							e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), i);
							e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), i);
						}
					}
				}
			}
		}
	}
}
