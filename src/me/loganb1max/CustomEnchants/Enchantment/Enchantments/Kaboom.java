package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Kaboom implements CustomEnchantment, Listener{

	private String name = "Kaboom";
	private String description = "Chance to summon a creeper upon death.";
	private Rarity rarity = Rarity.Rare;
	private Gear gear = Gear.Leggings;
	private int maxLvl = 3;
	
	public Kaboom() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if (ItemManager.hasEnchantment(getName(), p.getEquipment().getLeggings())) {
				if (p.getHealth() <= (p.getMaxHealth() * 0.2)) {
					int lvl = ItemManager.getEnchantmentLevel(getName(), p.getEquipment().getLeggings());
					Random rand = new Random();
					switch (lvl) {
					case 1:
						if (rand.nextInt(99)+1 < 10) {
							p.getWorld().spawnEntity(p.getLocation(), EntityType.CREEPER);
						}
					case 2:
						if (rand.nextInt(99)+1 < 20) {
							p.getWorld().spawnEntity(p.getLocation(), EntityType.CREEPER);
						}
					case 3:
						if (rand.nextInt(99)+1 < 30) {
							p.getWorld().spawnEntity(p.getLocation(), EntityType.CREEPER);
						}
					}
				}
			}
		}
	}
}
