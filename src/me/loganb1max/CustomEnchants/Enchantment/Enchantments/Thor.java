package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Thor implements CustomEnchantment, Listener{

	private String name = "Thor";
	private String description = "Chance to strike the opponent with lightning.";
	private Rarity rarity = Rarity.Rare;
	private Gear gear = Gear.Sword;
	private int maxLvl = 3;
	
	public Thor() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player) {
			Player p = (Player) e.getDamager();
			if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
				int lvl = ItemManager.getEnchantmentLevel(getName(), p.getItemInHand());
				Random rand = new Random();
				switch (lvl) {
				case 1:
					if (rand.nextInt(99)+1 < 7) {
						e.getEntity().getWorld().strikeLightning(e.getEntity().getLocation());
					}
				case 2:
					if (rand.nextInt(99)+1 < 14) {
						e.getEntity().getWorld().strikeLightning(e.getEntity().getLocation());
					}
				case 3:
					if (rand.nextInt(99)+1 < 21) {
						e.getEntity().getWorld().strikeLightning(e.getEntity().getLocation());
					}
				}
			}
		}
	}
}
