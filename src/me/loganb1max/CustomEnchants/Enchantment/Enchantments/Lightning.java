package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Lightning implements CustomEnchantment, Listener{

	private String name = "Lightning";
	private String description = "Chance to strike the opponent with lightning.";
	private Rarity rarity = Rarity.Rare;
	private Gear gear = Gear.Bow;
	private int maxLvl = 3;
	
	public Lightning() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onArrowDmg(EntityDamageByEntityEvent e) {
		if (e.getDamager().getType().equals(EntityType.ARROW)) {
			org.bukkit.entity.Arrow a = (org.bukkit.entity.Arrow) e.getDamager();
			if (a.getShooter() instanceof Player) {
				Player p = (Player) a.getShooter();
				if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
					int lvl = ItemManager.getEnchantmentLevel(getName(), p.getItemInHand());
					Random rand = new Random();
					switch (lvl) {
					case 1:
						if (rand.nextInt(99)+1 < 10) {
							e.getEntity().getWorld().strikeLightning(e.getEntity().getLocation());
						}
					case 2:
						if (rand.nextInt(99)+1 < 20) {
							e.getEntity().getWorld().strikeLightning(e.getEntity().getLocation());
						}
					case 3:
						if (rand.nextInt(99)+1 < 30) {
							e.getEntity().getWorld().strikeLightning(e.getEntity().getLocation());
						}
					}
				}
			}
			
		}
	}
}

