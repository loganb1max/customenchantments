package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Frozen implements CustomEnchantment, Listener{

	private String name = "Frozen";
	private String description = "Chance to give opponent slowness.";
	private Rarity rarity = Rarity.Legendary;
	private Gear gear = Gear.Sword;
	private int maxLvl = 3;
	
	public Frozen() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player) {
			Player p = (Player) e.getDamager();
			if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
				int lvl = ItemManager.getEnchantmentLevel(getName(), p.getItemInHand());
				if (e.getEntity() instanceof LivingEntity) {
					LivingEntity ent = (LivingEntity) e.getEntity();
					Random rand = new Random();
					switch (lvl) {
					case 1:
						if (rand.nextInt(99)+1 < 7) {
							ent.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 1, false, false));
						}
					case 2:
						if (rand.nextInt(99)+1 < 14) {
							ent.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 300, 1, false, false));
						}
					case 3:
						if (rand.nextInt(99)+1 < 21) {
							ent.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 400, 1, false, false));
						}
					case 4:
						if (rand.nextInt(99)+1 < 28) {
							ent.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 2, false, false));
						}
					case 5:
						if (rand.nextInt(99)+1 < 35) {
							ent.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 300, 2, false, false));
						}
					case 6:
						if (rand.nextInt(99)+1 < 42) {
							ent.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 400, 2, false, false));
						}
					}
				}
			}
		}
	}
}
