package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class AutoSmelt implements CustomEnchantment, Listener{

	private String name = "AutoSmelt";
	private String description = "AutoSmelts ores into gems";
	private Rarity rarity = Rarity.Basic;
	private Gear gear = Gear.Pickaxe;
	private int maxLvl = 1;
	
	public AutoSmelt() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
			HashMap<Material, Material> replacements = new HashMap<Material, Material>();
			replacements.put(Material.COBBLESTONE, Material.STONE);
			replacements.put(Material.SAND, Material.GLASS);
			replacements.put(Material.IRON_ORE, Material.IRON_INGOT);
			replacements.put(Material.GOLD_ORE, Material.GOLD_INGOT);
			if (replacements.containsKey(e.getBlock().getType())) {
				for (ItemStack i : e.getBlock().getDrops()) {
					if (replacements.containsKey(i.getType())) {
						if (ItemManager.hasEnchantment("Scavenger", p.getItemInHand())) {
							int lvl = ItemManager.getEnchantmentLevel("Scavenger", p.getItemInHand());
							Random rand = new Random();
							switch(lvl) {
							case 1:
								if (rand.nextInt(99)+1 < 10) {
										e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(replacements.get(i.getType()), i.getAmount()));
								}
							case 2:
								if (rand.nextInt(99)+1 < 20) {
										e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(replacements.get(i.getType()), i.getAmount()));
										e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(replacements.get(i.getType()), i.getAmount()));
								}
							case 3:
								if (rand.nextInt(99)+1 < 30) {
										e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(replacements.get(i.getType()), i.getAmount()));
										e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(replacements.get(i.getType()), i.getAmount()));
										e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(replacements.get(i.getType()), i.getAmount()));
								}
							}
							for (int s = 1; s <= lvl; s++) {
								e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(replacements.get(i.getType()), i.getAmount()));
							}
						} else {
							e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(replacements.get(i.getType()), i.getAmount()));
						}
					}
					if (ItemManager.hasEnchantment("Scavenger", p.getItemInHand())) {
						int lvl = ItemManager.getEnchantmentLevel("Scavenger", p.getItemInHand());
						Random rand = new Random();
						switch(lvl) {
						case 1:
							if (rand.nextInt(99)+1 < 10) {
									e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), i);
							}
						case 2:
							if (rand.nextInt(99)+1 < 20) {
									e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), i);
									e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), i);
							}
						case 3:
							if (rand.nextInt(99)+1 < 30) {
									e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), i);
									e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), i);
									e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), i);
							}
						}
					} else {
						e.getBlock().getWorld().dropItem(e.getBlock().getLocation(), new ItemStack(i.getType(), i.getAmount()));
					}
				}
			}
			e.setCancelled(true);
			e.getBlock().setType(Material.AIR);
		}
	}
}
