package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Karma implements CustomEnchantment, Listener{

	private String name = "Karma";
	private String description = "Chance to hit the other player when getting hit.";
	private Rarity rarity = Rarity.Legendary;
	private Gear gear = Gear.Chestplate;
	private int maxLvl = 4;
	
	public Karma() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p  = (Player) e.getEntity();
			if (e.getDamager() instanceof Player) {
				Player d = (Player) e.getDamager();
				ItemStack chestplate = p.getEquipment().getChestplate();
				Random rand = new Random();
				if (ItemManager.hasEnchantment(getName(), chestplate)) {
					int lvl = ItemManager.getEnchantmentLevel(getName(), chestplate);
					switch (lvl) {
					case 1:
						if (rand.nextInt(99)+1 < 15) {
							d.damage(e.getDamage());
						}
					case 2:
						if (rand.nextInt(99)+1 < 25) {
							d.damage(e.getDamage());
						}
					case 3:
						if (rand.nextInt(99)+1 < 35) {
							d.damage(e.getDamage());
						}
					case 4:
						if (rand.nextInt(99)+1 < 45) {
							d.damage(e.getDamage());
						}
					}
				}
			}
		}
	}
}
