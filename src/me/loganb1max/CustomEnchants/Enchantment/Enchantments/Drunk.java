package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Drunk implements CustomEnchantment{

	private String name = "Drunk";
	private String description = "Applies strength, mining fatigue, and slowness to your character.";
	private Rarity rarity = Rarity.Epic;
	private Gear gear = Gear.Helmet;
	private int maxLvl = 3;
	@SuppressWarnings("unused")
	private BukkitTask timer;
	
	public Drunk() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		startNightVisionTimer();
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	private void startNightVisionTimer() {
		timer = Bukkit.getScheduler().runTaskTimer(Main.getInstance(), new Runnable() {

			@Override
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
					ItemStack helmet = p.getEquipment().getHelmet();
					if (ItemManager.hasEnchantment(getName(), helmet)) {
						p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
						p.removePotionEffect(PotionEffectType.SLOW_DIGGING);
						p.removePotionEffect(PotionEffectType.SLOW);
						p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 400, ItemManager.getEnchantmentLevel(getName(), helmet)-1, false, false));
						p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 400, ItemManager.getEnchantmentLevel(getName(), helmet)-1, false, false));
						p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 400, ItemManager.getEnchantmentLevel(getName(), helmet)-1, false, false));
					}
				}
			}
			
		}, 0, 10);
	}

}
