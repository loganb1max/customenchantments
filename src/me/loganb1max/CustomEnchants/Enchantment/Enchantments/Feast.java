package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.logging.Level;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Feast implements CustomEnchantment, Listener{

	private String name = "Feast";
	private String description = "Prevents loss of saturation.";
	private Rarity rarity = Rarity.Basic;
	private Gear gear = Gear.Helmet;
	private int maxLvl = 1;
	
	public Feast() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(FoodLevelChangeEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p =  (Player) e.getEntity();
			if (ItemManager.hasEnchantment(getName(), p.getEquipment().getHelmet())) {
				p.setSaturation(p.getFoodLevel());
			}
		}
	}
}
