package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Ghost implements CustomEnchantment, Listener{

	private String name = "Ghost";
	private String description = "Chance to go invisible for 2 seconds when low health.";
	private Rarity rarity = Rarity.Legendary;
	private Gear gear = Gear.Chestplate;
	private int maxLvl = 1;
	
	public Ghost() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if (ItemManager.hasEnchantment(getName(), p.getEquipment().getLeggings())) {
				if (p.getHealth() <= (p.getMaxHealth() * 0.2)) {
					Random rand = new Random();
					if (rand.nextInt(99)+1 < 50) {
						for (Player s : Bukkit.getOnlinePlayers()) {
							s.hidePlayer(p);
						}
						Bukkit.getScheduler().runTaskLater(Main.getInstance(), new Runnable() {
							@Override
							public void run() {
								for (Player s : Bukkit.getOnlinePlayers()) {
									s.showPlayer(p);
								}
							}
						}, 40);
					}
				}
			}
		}
	}
}
