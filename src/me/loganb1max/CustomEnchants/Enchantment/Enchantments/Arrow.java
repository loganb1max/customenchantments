package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Arrow implements CustomEnchantment, Listener{

	private String name = "Arrow";
	private String description = "Chance to receive your arrows back.";
	private Rarity rarity = Rarity.Basic;
	private Gear gear = Gear.Bow;
	private int maxLvl = 5;
	
	public Arrow() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(ProjectileLaunchEvent e) {
		if (e.getEntity().getShooter() instanceof Player) {
			Player p = (Player) e.getEntity().getShooter();
			if (e.getEntityType().equals(EntityType.ARROW)) {
				if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
					int lvl = ItemManager.getEnchantmentLevel(getName(), p.getItemInHand());
					Random rand = new Random();
					switch (lvl) {
					case 1:
						if (rand.nextInt(99)+1 < 15) {
							p.getInventory().addItem(new ItemStack(Material.ARROW));
						}
					case 2:
						if (rand.nextInt(99)+1 < 30) {
							p.getInventory().addItem(new ItemStack(Material.ARROW));
						}
					case 3:
						if (rand.nextInt(99)+1 < 45) {
							p.getInventory().addItem(new ItemStack(Material.ARROW));
						}
					case 4:
						if (rand.nextInt(99)+1 < 60) {
							p.getInventory().addItem(new ItemStack(Material.ARROW));
						}
					case 5:
						if (rand.nextInt(99)+1 < 75) {
							p.getInventory().addItem(new ItemStack(Material.ARROW));
						}
					}
				}
			}
		}
	}
}
