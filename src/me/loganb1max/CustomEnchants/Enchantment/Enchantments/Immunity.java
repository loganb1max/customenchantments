package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Immunity implements CustomEnchantment{

	private String name = "Immunity";
	private String description = "Prevents disadvantageous potion effects.";
	private Rarity rarity = Rarity.Epic;
	private Gear gear = Gear.Helmet;
	private int maxLvl = 1;
	@SuppressWarnings("unused")
	private BukkitTask timer;
	
	public Immunity() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		startNightVisionTimer();
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	private void startNightVisionTimer() {
		timer = Bukkit.getScheduler().runTaskTimer(Main.getInstance(), new Runnable() {

			@Override
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
					ItemStack helmet = p.getEquipment().getHelmet();
					if (ItemManager.hasEnchantment(getName(), helmet)) {
						p.removePotionEffect(PotionEffectType.BLINDNESS);
						p.removePotionEffect(PotionEffectType.CONFUSION);
						p.removePotionEffect(PotionEffectType.HARM);
						p.removePotionEffect(PotionEffectType.HUNGER);
						p.removePotionEffect(PotionEffectType.POISON);
						p.removePotionEffect(PotionEffectType.SLOW);
						p.removePotionEffect(PotionEffectType.SLOW_DIGGING);
						p.removePotionEffect(PotionEffectType.WEAKNESS);
						p.removePotionEffect(PotionEffectType.WITHER);
					}
				}
			}
			
		}, 0, 10);
	}

}
