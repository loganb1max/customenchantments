package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class EXPThief implements CustomEnchantment, Listener{

	private String name = "EXPThief";
	private String description = "Chance to steal opponents EXP.";
	private Rarity rarity = Rarity.Epic;
	private Gear gear = Gear.Sword;
	private int maxLvl = 3;
	
	public EXPThief() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(EntityDeathEvent e) {
		if (e.getEntity().getKiller() instanceof Player) {
			Player p = (Player) e.getEntity().getKiller();
			if (e.getEntity() instanceof Player) {
				Player v = (Player) e.getEntity();
				if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
					int lvl = ItemManager.getEnchantmentLevel(getName(), p.getItemInHand());
					Random rand = new Random();
					switch (lvl) {
					case 1:
						if (rand.nextInt(99)+1 < 7) {
							p.setTotalExperience((int) (p.getTotalExperience() + (v.getTotalExperience() * 0.15)));
							v.setTotalExperience((int) (v.getTotalExperience() * 0.85));
						}
					case 2:
						if (rand.nextInt(99)+1 < 14) {
							p.setTotalExperience((int) (p.getTotalExperience() + (v.getTotalExperience() * 0.2)));
							v.setTotalExperience((int) (v.getTotalExperience() * 0.8));
						}
					case 3:
						if (rand.nextInt(99)+1 < 21) {
							p.setTotalExperience((int) (p.getTotalExperience() + (v.getTotalExperience() * 0.25)));
							v.setTotalExperience((int) (v.getTotalExperience() * 0.75));
						}
					}
				}
			}
		}
	}
}
