package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.logging.Level;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class EXP implements CustomEnchantment, Listener{

	private String name = "EXP";
	private String description = "Increase EXP dropped while mining.";
	private Rarity rarity = Rarity.Basic;
	private Gear gear = Gear.Pickaxe;
	private int maxLvl = 3;
	
	public EXP() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(BlockBreakEvent e) {
		Player p = e.getPlayer();
		if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
			e.setExpToDrop((int) (e.getExpToDrop() * (1 + (ItemManager.getEnchantmentLevel(getName(), e.getPlayer().getItemInHand()) * 0.1))));
		}
	}
}

