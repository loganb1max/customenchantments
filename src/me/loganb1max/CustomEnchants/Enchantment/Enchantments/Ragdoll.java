package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.util.Vector;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Ragdoll implements CustomEnchantment, Listener{

	private String name = "Ragdoll";
	private String description = "Chance to give the opponent extreme knockback.";
	private Rarity rarity = Rarity.Legendary;
	private Gear gear = Gear.Sword;
	private int maxLvl = 6;
	
	public Ragdoll() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player) {
			Player p = (Player) e.getDamager();
			if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
				int lvl = ItemManager.getEnchantmentLevel(getName(), p.getItemInHand());
				if (e.getEntity() instanceof LivingEntity) {
					LivingEntity ent = (LivingEntity) e.getEntity();
					Random rand = new Random();
					switch (lvl) {
					case 1:
						if (rand.nextInt(99)+1 < 7) {
							Vector vel = ent.getVelocity();
							vel.multiply(-2);
							ent.setVelocity(vel);
						}
					case 2:
						if (rand.nextInt(99)+1 < 14) {
							Vector vel = ent.getVelocity();
							vel.multiply(-2.5);
							ent.setVelocity(vel);
						}
					case 3:
						if (rand.nextInt(99)+1 < 21) {
							Vector vel = ent.getVelocity();
							vel.multiply(-3);
							ent.setVelocity(vel);
						}
					case 4:
						if (rand.nextInt(99)+1 < 28) {
							Vector vel = ent.getVelocity();
							vel.multiply(-3.5);
							ent.setVelocity(vel);
						}
					case 5:
						if (rand.nextInt(99)+1 < 35) {
							Vector vel = ent.getVelocity();
							vel.multiply(-4);
							ent.setVelocity(vel);
						}
					case 6:
						if (rand.nextInt(99)+1 < 42) {
							Vector vel = ent.getVelocity();
							vel.multiply(-5);
							ent.setVelocity(vel);
						}
					}
				}
			}
		}
	}
}
