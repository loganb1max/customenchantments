package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;
import net.milkbowl.vault.economy.Economy;

public class Thief implements CustomEnchantment, Listener{

	private String name = "Thief";
	private String description = "Chance to steal opponent�s money.";
	private Rarity rarity = Rarity.Rare;
	private Gear gear = Gear.Sword;
	private int maxLvl = 4;
	
	public Thief() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(PlayerDeathEvent e) {
		if (e.getEntity().getKiller() instanceof Player) {
			Player p = (Player) e.getEntity().getKiller();
			if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
				int lvl = ItemManager.getEnchantmentLevel(getName(), p.getItemInHand());
				Random rand = new Random();
				Economy econ = Main.getInstance().getEcon();
				switch (lvl) {
				case 1:
					if (rand.nextInt(99)+1 < 7) {
						econ.depositPlayer(p, econ.getBalance(e.getEntity())*0.1);
						econ.withdrawPlayer(e.getEntity(), econ.getBalance(e.getEntity())*0.1);
					}
				case 2:
					if (rand.nextInt(99)+1 < 14) {
						econ.depositPlayer(p, econ.getBalance(e.getEntity())*0.15);
						econ.withdrawPlayer(e.getEntity(), econ.getBalance(e.getEntity())*0.15);
					}
				case 3:
					if (rand.nextInt(99)+1 < 21) {
						econ.depositPlayer(p, econ.getBalance(e.getEntity())*0.2);
						econ.withdrawPlayer(e.getEntity(), econ.getBalance(e.getEntity())*0.2);
					}
				case 4:
					if (rand.nextInt(99)+1 < 28) {
						econ.depositPlayer(p, econ.getBalance(e.getEntity())*0.25);
						econ.withdrawPlayer(e.getEntity(), econ.getBalance(e.getEntity())*0.25);
					}
				}
			}
		}
	}
}
