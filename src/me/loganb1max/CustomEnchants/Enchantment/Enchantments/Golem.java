package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.IronGolem;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Golem implements CustomEnchantment{

	private String name = "Golem";
	private String description = "Chance to spawn golem guardians to protect you.";
	private Rarity rarity = Rarity.Epic;
	private Gear gear = Gear.Armour;
	private int maxLvl = 10;
	@SuppressWarnings("unused")
	private BukkitTask timer;
	
	public Golem() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		startNightVisionTimer();
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	private void startNightVisionTimer() {
		timer = Bukkit.getScheduler().runTaskTimer(Main.getInstance(), new Runnable() {

			@SuppressWarnings("unused")
			@Override
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
					ItemStack helmet = p.getEquipment().getHelmet();
					ItemStack chestplate = p.getEquipment().getChestplate();
					ItemStack leggings = p.getEquipment().getLeggings();
					ItemStack boots = p.getEquipment().getBoots();
					Random rand = new Random();
					if (ItemManager.hasEnchantment(getName(), helmet)) {
						int lvl = ItemManager.getEnchantmentLevel(getName(), helmet);
						switch (lvl) {
						case 1:
							if (rand.nextInt(99)+1 < 5) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 2:
							if (rand.nextInt(99)+1 < 10) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 3:
							if (rand.nextInt(99)+1 < 15) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 4:
							if (rand.nextInt(99)+1 < 20) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 5:
							if (rand.nextInt(99)+1 < 25) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 6:
							if (rand.nextInt(99)+1 < 30) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 7:
							if (rand.nextInt(99)+1 < 35) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 8:
							if (rand.nextInt(99)+1 < 40) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 9:
							if (rand.nextInt(99)+1 < 45) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 10:
							if (rand.nextInt(99)+1 < 50) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						}
					}
					if (ItemManager.hasEnchantment(getName(), chestplate)) {
						int lvl = ItemManager.getEnchantmentLevel(getName(), chestplate);
						switch (lvl) {
						case 1:
							if (rand.nextInt(99)+1 < 5) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 2:
							if (rand.nextInt(99)+1 < 10) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 3:
							if (rand.nextInt(99)+1 < 15) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 4:
							if (rand.nextInt(99)+1 < 20) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 5:
							if (rand.nextInt(99)+1 < 25) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 6:
							if (rand.nextInt(99)+1 < 30) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 7:
							if (rand.nextInt(99)+1 < 35) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 8:
							if (rand.nextInt(99)+1 < 40) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 9:
							if (rand.nextInt(99)+1 < 45) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 10:
							if (rand.nextInt(99)+1 < 50) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						}
					}
					if (ItemManager.hasEnchantment(getName(), leggings)) {
						int lvl = ItemManager.getEnchantmentLevel(getName(), leggings);
						switch (lvl) {
						case 1:
							if (rand.nextInt(99)+1 < 5) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 2:
							if (rand.nextInt(99)+1 < 10) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 3:
							if (rand.nextInt(99)+1 < 15) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 4:
							if (rand.nextInt(99)+1 < 20) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 5:
							if (rand.nextInt(99)+1 < 25) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 6:
							if (rand.nextInt(99)+1 < 30) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 7:
							if (rand.nextInt(99)+1 < 35) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 8:
							if (rand.nextInt(99)+1 < 40) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 9:
							if (rand.nextInt(99)+1 < 45) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 10:
							if (rand.nextInt(99)+1 < 50) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						}
					}
					if (ItemManager.hasEnchantment(getName(), boots)) {
						int lvl = ItemManager.getEnchantmentLevel(getName(), boots);
						switch (lvl) {
						case 1:
							if (rand.nextInt(99)+1 < 5) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 2:
							if (rand.nextInt(99)+1 < 10) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 3:
							if (rand.nextInt(99)+1 < 15) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 4:
							if (rand.nextInt(99)+1 < 20) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 5:
							if (rand.nextInt(99)+1 < 25) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 6:
							if (rand.nextInt(99)+1 < 30) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 7:
							if (rand.nextInt(99)+1 < 35) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 8:
							if (rand.nextInt(99)+1 < 40) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 9:
							if (rand.nextInt(99)+1 < 45) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						case 10:
							if (rand.nextInt(99)+1 < 50) {
								IronGolem g = (IronGolem) p.getWorld().spawnEntity(p.getLocation(), EntityType.IRON_GOLEM);
							}
						}
					}
				}
			}
			
		}, 0, 1200);
	}

}
