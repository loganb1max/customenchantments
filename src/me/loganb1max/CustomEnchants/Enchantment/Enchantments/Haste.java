package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Haste implements CustomEnchantment{

	private String name = "Haste";
	private String description = "Applies haste while holding.";
	private Rarity rarity = Rarity.Rare;
	private Gear gear = Gear.Pickaxe;
	private int maxLvl = 3;
	@SuppressWarnings("unused")
	private BukkitTask timer;
	
	public Haste() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		startNightVisionTimer();
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	private void startNightVisionTimer() {
		timer = Bukkit.getScheduler().runTaskTimer(Main.getInstance(), new Runnable() {

			@Override
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
					if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
						p.removePotionEffect(PotionEffectType.FAST_DIGGING);
						p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 400, ItemManager.getEnchantmentLevel(getName(), p.getItemInHand())-1, false, false));
					}
				}
			}
			
		}, 0, 10);
	}

}
