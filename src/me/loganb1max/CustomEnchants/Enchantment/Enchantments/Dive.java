package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.logging.Level;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Dive implements CustomEnchantment, Listener{

	private String name = "Dive";
	private String description = "Increases speed in water.";
	private Rarity rarity = Rarity.Rare;
	private Gear gear = Gear.Boots;
	private int maxLvl = 1;
	
	public Dive() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(PlayerMoveEvent e) {
		Player p = e.getPlayer();
		if (ItemManager.hasEnchantment(getName(), e.getPlayer().getEquipment().getBoots())) {
			if (p.getWorld().getBlockAt(p.getLocation().add(0, -1, 0)).getType().equals(Material.WATER) || p.getWorld().getBlockAt(p.getLocation().add(0, -1, 0)).getType().equals(Material.STATIONARY_WATER)) {
				p.setWalkSpeed((float) 0.4);
			} else {
				p.setWalkSpeed((float) 0.2);
			}
		}
	}
}
