package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.logging.Level;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Confirmed implements CustomEnchantment, Listener{

	private String name = "Confirmed";
	private String description = "Gives you regeneration when you kill a player.";
	private Rarity rarity = Rarity.Legendary;
	private Gear gear = Gear.Sword;
	private int maxLvl = 3;
	
	public Confirmed() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(EntityDeathEvent e) {
		if (e.getEntity().getKiller() instanceof Player) {
			Player p = (Player) e.getEntity().getKiller();
			if (e.getEntity() instanceof Player) {
				if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
					int lvl = ItemManager.getEnchantmentLevel(getName(), p.getItemInHand());
					switch (lvl) {
					case 1:
						p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 200, 1, false, false));
					case 2:
						p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 200, 2, false, false));
					case 3:
						p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 200, 3, false, false));
					}
				}
			}
		}
	}
}
