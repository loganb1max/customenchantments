package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class LifeSteal implements CustomEnchantment, Listener{

	private String name = "LifeSteal";
	private String description = "Chance to steal some of the enemy�s health.";
	private Rarity rarity = Rarity.Legendary;
	private Gear gear = Gear.Sword;
	private int maxLvl = 3;
	
	public LifeSteal() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player) {
			Player p = (Player) e.getDamager();
			if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
				int lvl = ItemManager.getEnchantmentLevel(getName(), p.getItemInHand());
				Random rand = new Random();
				switch (lvl) {
				case 1:
					if (rand.nextInt(99)+1 < 20) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				case 2:
					if (rand.nextInt(99)+1 < 40) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				case 3:
					if (rand.nextInt(99)+1 < 60) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				}
			}
		}
	}
}
