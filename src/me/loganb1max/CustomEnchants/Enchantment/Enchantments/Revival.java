package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.Random;
import java.util.logging.Level;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Revival implements CustomEnchantment, Listener{

	private String name = "Revival";
	private String description = "Chance to receive health back.";
	private Rarity rarity = Rarity.Epic;
	private Gear gear = Gear.Armour;
	private int maxLvl = 3;
	
	public Revival() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}

	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p  = (Player) e.getEntity();
			ItemStack helmet = p.getEquipment().getHelmet();
			ItemStack chestplate = p.getEquipment().getChestplate();
			ItemStack leggings = p.getEquipment().getLeggings();
			ItemStack boots = p.getEquipment().getBoots();
			Random rand = new Random();
			if (ItemManager.hasEnchantment(getName(), helmet)) {
				int lvl = ItemManager.getEnchantmentLevel(getName(), helmet);
				switch (lvl) {
				case 1:
					if (rand.nextInt(99)+1 < 15) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				case 2:
					if (rand.nextInt(99)+1 < 25) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				case 3:
					if (rand.nextInt(99)+1 < 35) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				}
			}
			if (ItemManager.hasEnchantment(getName(), chestplate)) {
				int lvl = ItemManager.getEnchantmentLevel(getName(), chestplate);
				switch (lvl) {
				case 1:
					if (rand.nextInt(99)+1 < 15) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				case 2:
					if (rand.nextInt(99)+1 < 25) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				case 3:
					if (rand.nextInt(99)+1 < 35) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				}
			}
			if (ItemManager.hasEnchantment(getName(), leggings)) {
				int lvl = ItemManager.getEnchantmentLevel(getName(), leggings);
				switch (lvl) {
				case 1:
					if (rand.nextInt(99)+1 < 15) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				case 2:
					if (rand.nextInt(99)+1 < 25) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				case 3:
					if (rand.nextInt(99)+1 < 35) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				}
			}
			if (ItemManager.hasEnchantment(getName(), boots)) {
				int lvl = ItemManager.getEnchantmentLevel(getName(), boots);
				switch (lvl) {
				case 1:
					if (rand.nextInt(99)+1 < 15) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				case 2:
					if (rand.nextInt(99)+1 < 25) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				case 3:
					if (rand.nextInt(99)+1 < 35) {
						if (p.getHealth() + e.getDamage() <= p.getMaxHealth()) {
							p.setHealth(p.getHealth() + e.getDamage());
						} else {
							p.setHealth(p.getMaxHealth());
						}
					}
				}
			}
		}
	}
}
