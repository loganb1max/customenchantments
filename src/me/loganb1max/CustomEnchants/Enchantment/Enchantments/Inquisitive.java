package me.loganb1max.CustomEnchants.Enchantment.Enchantments;

import java.util.logging.Level;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Items.ItemManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;
import me.loganb1max.CustomEnchants.Objects.Gear;
import me.loganb1max.CustomEnchants.Objects.Rarity;

public class Inquisitive implements CustomEnchantment, Listener{

	private String name = "Inquisitive";
	private String description = "Increases the EXP dropped while grinding.";
	private Rarity rarity = Rarity.Epic;
	private Gear gear = Gear.Sword;
	private int maxLvl = 5;
	
	public Inquisitive() {
		Main.getInstance().getEnchantmentManager().registerEnchantment(this);
		Main.getInstance().getLogger().log(Level.INFO, "CUSTOM ENCHANTMENTS: ENCHANTMENT " + getName() + " REGISTERED!");
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getMaxLevel() {
		return maxLvl;
	}

	@Override
	public Rarity getRarity() {
		return rarity;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Gear getGear() {
		return gear;
	}
	
	@EventHandler
	public void onApplyEXP(EntityDeathEvent e) {
		if (e.getEntity().getKiller() instanceof Player) {
			Player p = (Player) e.getEntity().getKiller();
			if (ItemManager.hasEnchantment(getName(), p.getItemInHand())) {
				int lvl = ItemManager.getEnchantmentLevel(getName(), p.getItemInHand());
				switch (lvl) {
				case 1:
					e.setDroppedExp((int) (e.getDroppedExp() * 1.25));
				case 2:
					e.setDroppedExp((int) (e.getDroppedExp() * 1.50));
				case 3:
					e.setDroppedExp((int) (e.getDroppedExp() * 1.75));
				case 4:
					e.setDroppedExp((int) (e.getDroppedExp() * 2.0));
				case 5:
					e.setDroppedExp((int) (e.getDroppedExp() * 2.25));
				}
			}
		}
	}
}
