package me.loganb1max.CustomEnchants.GUI;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.loganb1max.CustomEnchants.Objects.TierBook;

public  class EnchanterGUI implements Listener{
	
	@SuppressWarnings("deprecation")
	public static void showEnchantGui(Player p) {
		Inventory gui = Bukkit.createInventory(null, 27, ChatColor.RED + "Enchanter");
		ItemStack tier1Book = new ItemStack(Material.BOOK);
		ItemMeta tier1BookMeta = tier1Book.getItemMeta();
		tier1BookMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		tier1BookMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES);
		tier1BookMeta.setDisplayName(ChatColor.YELLOW.toString() + "Purchase" + ChatColor.GRAY.toString() + " � " + ChatColor.AQUA + "Basic Book");
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.RED.toString() + "Price: " + ChatColor.GRAY + "2500 EXP");
		tier1BookMeta.setLore(lore);
		tier1Book.setItemMeta(tier1BookMeta);
		
		ItemStack tier2Book = new ItemStack(Material.BOOK);
		ItemMeta tier2BookMeta = tier2Book.getItemMeta();
		tier2BookMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		tier2BookMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES);
		tier2BookMeta.setDisplayName(ChatColor.YELLOW.toString() + "Purchase" + ChatColor.GRAY.toString() + " � " + ChatColor.GREEN + "Rare Book");
		List<String> lore2 = new ArrayList<String>();;
		lore2.add(ChatColor.RED.toString() + "Price: " + ChatColor.GRAY + "5000 EXP");
		tier2BookMeta.setLore(lore2);
		tier2Book.setItemMeta(tier2BookMeta);
		
		ItemStack tier3Book = new ItemStack(Material.BOOK);
		ItemMeta tier3BookMeta = tier3Book.getItemMeta();
		tier3BookMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		tier3BookMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES);
		tier3BookMeta.setDisplayName(ChatColor.YELLOW.toString() + "Purchase" + ChatColor.GRAY.toString() + " � " + ChatColor.GOLD + "Epic Book");
		List<String> lore3 = new ArrayList<String>();
		lore3.add(ChatColor.RED.toString() + "Price: " + ChatColor.GRAY + "15000 EXP");
		tier3BookMeta.setLore(lore3);
		tier3Book.setItemMeta(tier3BookMeta);
		
		ItemStack tier4Book = new ItemStack(Material.BOOK);
		ItemMeta tier4BookMeta = tier4Book.getItemMeta();
		tier4BookMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		tier4BookMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES);
		tier4BookMeta.setDisplayName(ChatColor.YELLOW.toString() + "Purchase" + ChatColor.GRAY.toString() + " � " + ChatColor.RED + "Legendary Book");
		List<String> lore4 = new ArrayList<String>();
		lore4.add(ChatColor.RED.toString() + "Price: " + ChatColor.GRAY + "50000 EXP");
		tier4BookMeta.setLore(lore4);
		tier4Book.setItemMeta(tier4BookMeta);
		
		gui.setItem(10, tier1Book);
		gui.setItem(12, tier2Book);
		gui.setItem(14, tier3Book);
		gui.setItem(16, tier4Book);
		
		ItemStack filler = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.GRAY.getData());
		ItemMeta fillerMeta = filler.getItemMeta();
		fillerMeta.setDisplayName(" ");
		fillerMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		fillerMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_ATTRIBUTES);
		filler.setItemMeta(fillerMeta);		
		for (int i = 0; i < gui.getSize(); i++) {
			if (gui.getItem(i) == null) {
				gui.setItem(i, filler);
			}
		}
		p.openInventory(gui);
	}
	
	
	@EventHandler
	public void onGuiInteract(InventoryClickEvent e) {
		if (e.getWhoClicked() instanceof Player) {
			Player p = (Player) e.getWhoClicked();
			if (e.getSlotType().equals(SlotType.OUTSIDE)) {
				return;
			}
			if (e.getCurrentItem().hasItemMeta() && e.getClickedInventory().getName().equals(ChatColor.RED + "Enchanter")) {
				if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Basic")) {
					if (p.getTotalExperience() >= 2500) {
						p.setTotalExperience(p.getTotalExperience()-2500);
						p.updateInventory();
						p.getInventory().addItem(TierBook.getTier1Book());
						p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
					} else if (p.getTotalExperience() < 2500) {
						p.sendMessage(ChatColor.RED + "You are missing " + (int)(2500.0-p.getTotalExperience()) + " exp.");
						p.closeInventory();
					}
				} else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Rare")) {
					if (p.getTotalExperience() >= 5000) {
						p.setTotalExperience(p.getTotalExperience()-5000);
						p.updateInventory();
						p.getInventory().addItem(TierBook.getTier2Book());
						p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
					} else if (p.getTotalExperience() < 5000) {
						p.sendMessage(ChatColor.RED + "You are missing " + (int)(5000.0-p.getTotalExperience()) + " exp.");
						p.closeInventory();
					}
				} else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Epic")) {
					if (p.getTotalExperience() >= 15000) {
						p.setTotalExperience(p.getTotalExperience()-15000);
						p.updateInventory();
						p.getInventory().addItem(TierBook.getTier3Book());
						p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
					} else if (p.getTotalExperience() < 15000) {
						p.sendMessage(ChatColor.RED + "You are missing " + (int)(15000.0-p.getTotalExperience()) + " exp.");
						p.closeInventory();
					}
				} else if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Legendary")) {
					if (p.getTotalExperience() >= 50000) {
						p.setTotalExperience(p.getTotalExperience()-50000);
						p.updateInventory();
						p.getInventory().addItem(TierBook.getTier4Book());
						p.playSound(p.getLocation(), Sound.ORB_PICKUP, 1, 1);
					} else if (p.getTotalExperience() < 50000) {
						p.sendMessage(ChatColor.RED + "You are missing " + (int)(50000.0-p.getTotalExperience()) + " exp.");
						p.closeInventory();
					}
				}
				e.setCancelled(true);
			}
		}
	}
	
	
	
}
