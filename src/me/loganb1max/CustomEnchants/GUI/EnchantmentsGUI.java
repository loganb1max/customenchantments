package me.loganb1max.CustomEnchants.GUI;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import me.loganb1max.CustomEnchants.Main;
import me.loganb1max.CustomEnchants.Enchantment.EnchantmentManager;
import me.loganb1max.CustomEnchants.Objects.CustomEnchantment;

@SuppressWarnings("deprecation")
public class EnchantmentsGUI implements Listener{
	private static Inventory inventory = Bukkit.createInventory(null, 36, ChatColor.GRAY + "Enchantments - Page 1 of 2");
	
	private static ItemStack getBackButton() {
		ItemStack back = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.RED.getData());
		ItemMeta backMeta = back.getItemMeta();
		backMeta.setDisplayName(ChatColor.GRAY.toString() + "Click to view the " + ChatColor.RED + "Previous Page" + ChatColor.GRAY + ".");
		back.setItemMeta(backMeta);
		return back;
	}
	
	private static ItemStack getNextButton() {
		ItemStack next = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.GREEN.getData());
		ItemMeta nextMeta = next.getItemMeta();
		nextMeta.setDisplayName(ChatColor.GRAY.toString() + "Click to view the " + ChatColor.GREEN + "Next Page" + ChatColor.GRAY + ".");
		next.setItemMeta(nextMeta);
		return next;
	}
	
	private static ItemStack getCurrentTile(int pageNum) {
		ItemStack current = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.ORANGE.getData());
		ItemMeta currentMeta = current.getItemMeta();
		currentMeta.setDisplayName(ChatColor.GRAY.toString() + "Page " + ChatColor.GOLD + pageNum + ChatColor.GRAY + " of " + ChatColor.GOLD + "2" + ChatColor.GRAY + ".");
		current.setItemMeta(currentMeta);
		return current;
	}
	
	private static ItemStack getFiller() {
		ItemStack fill = new ItemStack(Material.STAINED_GLASS_PANE, 1, DyeColor.GRAY.getData());
		ItemMeta fillMeta = fill.getItemMeta();
		fillMeta.setDisplayName(" ");
		fill.setItemMeta(fillMeta);
		return fill;
	}
	
	public static void openEnchantmentsGui(Player p) {
		loadPage(1, p);
	}
	
	@SuppressWarnings("unused")
	private static void loadPage(int pageNum, Player p) {
		if (pageNum == 1) {
			Inventory old = inventory;
			inventory = Bukkit.createInventory(null, 36, ChatColor.GRAY + "Enchantments - Page 1 of 2");
			inventory.setItem(31, getCurrentTile(1));
			for (int i = 27; i < 36; i++) {
				if (i != 31) {
					inventory.setItem(i, getFiller());
				}
			}
			inventory.setItem(35, getNextButton());
			EnchantmentManager man = Main.getInstance().getEnchantmentManager();
			ArrayList<CustomEnchantment> ces = Main.getInstance().getEnchantmentManager().getEnchantmentsList();
			for (int i = 0; i < 27; i++) {
				CustomEnchantment ce = ces.get(i);
				inventory.setItem(i, man.generateEnchantmentInfoCard(ce.getName(), ce.getMaxLevel(), ce.getRarity(), ce.getDescription(), ce.getGear()));
			}
			p.openInventory(inventory);
		} else if (pageNum == 2) {
			Inventory old = inventory;
			inventory = Bukkit.createInventory(null, 36, ChatColor.GRAY + "Enchantments - Page 2 of 2");
			inventory.setItem(31, getCurrentTile(1));
			for (int i = 27; i < 35; i++) {
				if (i != 31) {
					inventory.setItem(i, getFiller());
				}
			}
			inventory.setItem(27, getBackButton());
			EnchantmentManager man = Main.getInstance().getEnchantmentManager();
			ArrayList<CustomEnchantment> ces = Main.getInstance().getEnchantmentManager().getEnchantmentsList();
			int s = 27;
			for (int i = 0; i < ces.size()-27; i++) {
				CustomEnchantment ce = ces.get(s);
				inventory.setItem(i, man.generateEnchantmentInfoCard(ce.getName(), ce.getMaxLevel(), ce.getRarity(), ce.getDescription(), ce.getGear()));
				s++;
			}
			p.openInventory(inventory);
		}
	}
	
	@EventHandler
	public void onGuiInteract(InventoryClickEvent e) {
		if (e.getWhoClicked() instanceof Player) {
			Player p = (Player) e.getWhoClicked();
			
			if (e.getSlotType().equals(SlotType.OUTSIDE)) {
				return;
			}
			
			if (e.getCurrentItem().hasItemMeta() && e.getClickedInventory().getName().equals(ChatColor.GRAY + "Enchantments - Page 1 of 2")) {
				if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Next")) {
					loadPage(2, p);
				}
				e.setCancelled(true);
			}
			
			if (e.getCurrentItem().hasItemMeta() && e.getClickedInventory().getName().equals(ChatColor.GRAY + "Enchantments - Page 2 of 2")) {
				if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Previous")) {
					loadPage(1, p);
				}
				e.setCancelled(true);
			}
		}
	}
}
