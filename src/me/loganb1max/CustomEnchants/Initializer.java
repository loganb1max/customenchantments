package me.loganb1max.CustomEnchants;

import java.util.logging.Level;

import org.bukkit.Bukkit;

import me.loganb1max.CustomEnchants.Commands.CustomEnchantCommand;
import me.loganb1max.CustomEnchants.Commands.CustomEnchantsCommand;
import me.loganb1max.CustomEnchants.Enchantment.EnchantmentManager;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Angel;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Aquatic;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Archer;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Arrow;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.ArrowSteal;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.AutoSmelt;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.BlastImmunity;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Boost;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Confirmed;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Dive;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.DoubleStrike;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Drunk;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.EXP;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.EXPThief;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Feast;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Frozen;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Ghost;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Golem;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Hardened;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Haste;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Hunger;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Immunity;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Inquisitive;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.JellyLegs;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Kaboom;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Karma;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.KillAura;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.LifeSteal;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Lightning;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.NightVision;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Ninja;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.ObsidianDestroyer;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Penetrate;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Pyro;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Ragdoll;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Rage;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Replenish;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Revival;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Scavenger;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Silence;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Slingshot;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Speed;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Strength;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Thief;
import me.loganb1max.CustomEnchants.Enchantment.Enchantments.Thor;
import me.loganb1max.CustomEnchants.GUI.EnchanterGUI;
import me.loganb1max.CustomEnchants.GUI.EnchantmentsGUI;
import me.loganb1max.CustomEnchants.Items.ItemManager;

public class Initializer {
	private Main instance;
	
	public Initializer(Main main) {
		this.instance = main;
	}
	
	public void registerListeners() {
		Bukkit.getPluginManager().registerEvents(new EnchanterGUI(), instance);
		Bukkit.getPluginManager().registerEvents(new EnchantmentManager(), instance);
		Bukkit.getPluginManager().registerEvents(new EnchantmentsGUI(), instance);
		Bukkit.getPluginManager().registerEvents(new ItemManager(), instance);
		
		instance.getLogger().log(Level.INFO, "CUSTOM ENCHANMENTS: LISTENERS REGISTERED!");
	}
	
	public void registerCommands() {
		instance.getCommand("enchanter").setExecutor(new CustomEnchantCommand());
		instance.getCommand("enchants").setExecutor(new CustomEnchantsCommand());
		
		instance.getLogger().log(Level.INFO, "CUSTOM ENCHANMENTS: COMMANDS REGISTERED!");
	}
	
	public void registerEnchantments() {
		new NightVision();
		Bukkit.getPluginManager().registerEvents(new EXP(), instance);
		Bukkit.getPluginManager().registerEvents(new Scavenger(), instance);
		Bukkit.getPluginManager().registerEvents(new Feast(), instance);
		new Aquatic();
		Bukkit.getPluginManager().registerEvents(new Arrow(), instance);
		Bukkit.getPluginManager().registerEvents(new AutoSmelt(), instance);
		Bukkit.getPluginManager().registerEvents(new Thor(), instance);
		Bukkit.getPluginManager().registerEvents(new Kaboom(), instance);
		Bukkit.getPluginManager().registerEvents(new DoubleStrike(), instance);
		Bukkit.getPluginManager().registerEvents(new Penetrate(), instance);
		new Haste();
		Bukkit.getPluginManager().registerEvents(new Dive(), instance);
		Bukkit.getPluginManager().registerEvents(new Thief(), instance);
		Bukkit.getPluginManager().registerEvents(new BlastImmunity(), instance);
		new Strength();
		Bukkit.getPluginManager().registerEvents(new Lightning(), instance);
		Bukkit.getPluginManager().registerEvents(new ObsidianDestroyer(), instance);
		Bukkit.getPluginManager().registerEvents(new Ninja(), instance);
		new Pyro();
		new Drunk();
		Bukkit.getPluginManager().registerEvents(new KillAura(), instance);
		new Immunity();
		new Boost();
		Bukkit.getPluginManager().registerEvents(new Archer(), instance);
		new Golem();
		Bukkit.getPluginManager().registerEvents(new EXPThief(), instance);
		Bukkit.getPluginManager().registerEvents(new Silence(), instance);
		Bukkit.getPluginManager().registerEvents(new ArrowSteal(), instance);
		new Replenish();
		Bukkit.getPluginManager().registerEvents(new Angel(), instance);
		Bukkit.getPluginManager().registerEvents(new Inquisitive(), instance);
		new Speed();
		Bukkit.getPluginManager().registerEvents(new Rage(), instance);
		Bukkit.getPluginManager().registerEvents(new Revival(), instance);
		Bukkit.getPluginManager().registerEvents(new JellyLegs(), instance);
		new Hardened();
		Bukkit.getPluginManager().registerEvents(new LifeSteal(), instance);
		Bukkit.getPluginManager().registerEvents(new Ghost(), instance);
		Bukkit.getPluginManager().registerEvents(new Confirmed(), instance);
		Bukkit.getPluginManager().registerEvents(new Karma(), instance);
		Bukkit.getPluginManager().registerEvents(new Frozen(), instance);
		Bukkit.getPluginManager().registerEvents(new Slingshot(), instance);
		Bukkit.getPluginManager().registerEvents(new Hunger(), instance);
		Bukkit.getPluginManager().registerEvents(new Ragdoll(), instance);
		
		instance.getLogger().log(Level.INFO, "CUSTOM ENCHANMENTS: ENCHANTMENTS REGISTERED!");
	}
}
